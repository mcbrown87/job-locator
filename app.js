require('dotenv').config()
const express = require('express')
const path = require('path')
const Auth0Strategy = require('passport-auth0');
const session = require('express-session');
const passport = require('passport');
const mongoose = require('mongoose')
const url = require('url');
const querystring = require('querystring');
var bodyParser = require('body-parser');
const ManagementClient = require('auth0').ManagementClient;
const util = require('util');
mongoose.connect(`${process.env.MONGO_URL}`, { useNewUrlParser: true })
const app = express()
const Job = require('./models/Job')

var sess = {
    secret: process.env.SESSION_SECRET,
    cookie: {},
    resave: false,
    saveUninitialized: true
  };

app.use(session(sess));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static('client/build'))
app.use(bodyParser.json());

const port = process.env.PORT ? process.env.PORT : 5000

// Configure Passport to use Auth0
var strategy = new Auth0Strategy(
    {
      domain: process.env.AUTH_DOMAIN,
      clientID: process.env.AUTH_CLIENT_ID,
      clientSecret: process.env.AUTH_CLIENT_SECRET,
      callbackURL:
        process.env.AUTH_CALLBACK_URL || 'http://localhost:5000/callback'
    },
    function (accessToken, refreshToken, extraParams, profile, done) {
      // accessToken is the token to call Auth0 API (not needed in the most cases)
      // extraParams.id_token has the JSON Web Token
      // profile has all the information from the user

      return done(null, profile);
    }
  );
  
  passport.use(strategy);
  
  // You can use this section to keep a smaller payload
  passport.serializeUser(function (user, done) {
    done(null, user);
  });
  
  passport.deserializeUser(function (user, done) {
    done(null, user);
  });

function isAdmin(userId) {
  let admins = process.env.ADMINS.split(',')
  return admins.includes(userId)
}

// Perform the login, after login Auth0 will redirect to callback
app.get('/login', passport.authenticate('auth0', {
    scope: 'openid email profile'
  }), function (req, res) {
    res.redirect('/');
  });
  
  // Perform the final stage of authentication and redirect to previously requested URL or '/user'
  app.get('/callback', function (req, res, next) {
    passport.authenticate('auth0', function (err, user, info) {
      if (err) { return next(err); }
      if (!user) { return res.redirect('/login'); }
      req.logIn(user, function (err) {
        if (err) { return next(err); }
        const returnTo = req.session.returnTo;
        delete req.session.returnTo;
        res.redirect(returnTo || '/');
      });
    })(req, res, next);
  });
  
  // Perform session logout and redirect to homepage
  app.get('/logout', (req, res) => {
    req.logout();
  
    var returnTo = process.env.SERVER_NAME
  
    var logoutURL = new url.URL(
      util.format('https://%s/v2/logout', process.env.AUTH_DOMAIN)
    );
    var searchString = querystring.stringify({
      client_id: process.env.AUTH_CLIENT_ID,
      returnTo: returnTo
    });
    logoutURL.search = searchString;
  
    res.redirect(logoutURL);
  });  

app.get('/jobs', async (req, res) => {
    try {
        let jobs = await Job.find({})
        res.send(jobs)
    } catch (e) {
        console.log(e)
        res.sendStatus(500)
    }
})

app.get('/getJobsite', async (req, res) => {
    try {
      let job = await Job.findOne({employees: req.user.id})
      if(job) {
        res.send(job.location)
      } else {
        res.send("")
      }
      
  } catch (e) {
      console.log(e)
      res.sendStatus(500)
  }
})

app.post('/newJob', async (req, res) => {
    try {
        let newJob = req.body
        if(await Job.exists({ name: newJob.jobName })) {
          await Job.updateOne({name: newJob.jobName}, {
            startDate: newJob.jobStartDate,
            endDate: newJob.jobEndDate,
            employees: newJob.employees,
            location: newJob.jobLocation
          })
          res.statusCode = 201
          res.send("Job updated");
          return
        }
        const job = new Job({
            _id: mongoose.Types.ObjectId(),
            name: newJob.jobName,
            startDate: newJob.jobStartDate,
            endDate: newJob.jobEndDate,
            employees: newJob.employees,
            location: newJob.jobLocation
        })

        await job.save()
        res.statusCode = 201
        res.send("Job created");
    } catch (e) {
        console.log(e)
        res.sendStatus(500)
    }
})

app.get('/getUserInfo', async (req, res) => {
    try {
        res.send(req.user)
    } catch(e) {
        console.log(e)
        res.sendStatus(500) 
    }
})

app.get('/getUsers', async (req, res) => {
  try {
    if(!isAdmin(req.user.id)) {
      res.statusCode = 401
      res.send("Not authorized");
      return
    }
    
    var auth0 = new ManagementClient({
      domain: process.env.AUTH_DOMAIN,
      clientId: process.env.AUTH_MGMT_CLIENT_ID,
      clientSecret: process.env.AUTH_MGMT_CLIENT_SECRET,
      scope: 'read:users'
    })
  
    let users = await auth0.getUsers()
    res.send(users)
  } catch (e) {
    console.log(e)
    res.sendStatus(500)
  }
})

app.get('/isAdmin', async (req, res) => {
  try {
    res.send(isAdmin(req.user.id))
  } catch (e) {
    console.log(e)
    res.sendStatus(500)
  }
})

app.get('/', async (req, res) => {
    try {
        if(req.user) {
            res.sendFile(path.join(__dirname, 'client/build/app.html'))
        } else {
            res.redirect('login')
        }
    } catch(e) {
        console.log(e)
        res.sendStatus(500)
    }
})

app.listen(port, () => console.log(`App listening on port ${port}!`))