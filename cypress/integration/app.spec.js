/// <reference types="Cypress" />

describe("Job locator", () => {
  beforeEach(function () {
    cy.visit(Cypress.env("SERVER_NAME"))
    cy.url().then(url => {
      if(url.includes('login')) {
        cy.get('input[name=email]').type(Cypress.env("USER_NAME"))
        cy.get('input[name=password]').type(Cypress.env("PASSWORD"), { log: false })
        cy.get('button[name=submit]').click()
        cy.get('body').then(($body) => {
          // synchronously query from body
          // to find which element was created
          if ($body.find('#allow').length) {
            // input was found, do something else here
            cy.get('#allow').click()
          }
        })
      }   
    })
  })
  
  it('Should have correct title', () => {
    cy.title().should('eq', 'Job Locator')
  })

  it('Should have correct welcome text', () => {
    cy.get('#welcomeId').should('contain', 'test@test.com')
  })

  it('Should redirect to login screen after logout', () => {
    cy.get('#logoutId').click()
    cy.url().should('contain', 'login')
  })
})