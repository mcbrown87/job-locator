import React from 'react';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import ManpowerPortal from './ManpowerPortal'
import AdminPortal from './AdminPortal'
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import Button from '@material-ui/core/Button'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import './App.css';

export default class App extends React.Component {
  constructor() {
    super()

    this.state = {
      navSelection: 0,
      isAdmin: false
    }

    this.logout = this.logout.bind(this)

    fetch('isAdmin')
    .then(res => res.json()) 
    .then(res => {
      this.setState(() => ({
        isAdmin: res
      })) 
    }) 
  }

  logout () {
    window.location.href = "/logout"
  }

  render() {
    return (
      <div className="App">
        <AppBar position="static">
          <Toolbar>
            <Button 
              color="inherit"
              onClick={this.logout}
              id="logoutId"
              >Logout</Button>
          </Toolbar>
        </AppBar>
        {this.state.navSelection === 0 ? <ManpowerPortal /> : null}
        {this.state.navSelection === 1 ? <AdminPortal id="adminPortal"/> : null}
        <Grid container 
          direction="column"
          justify="center"
          alignItems="center">
          <BottomNavigation
            id='bottomNav'
            value={this.state.navSelection}
            onChange={(event, newValue) => {
              this.setState(() => ({
                navSelection: newValue
              })) 
            }}
            showLabels
          >
            <BottomNavigationAction label="ManpowerPortal" icon={<EmojiPeopleIcon />} />
            {this.state.isAdmin ? <BottomNavigationAction label="Administration" icon={<SupervisorAccountIcon />} /> : null}
          </BottomNavigation>    
        </Grid>
      </div>
    )
  }
}
