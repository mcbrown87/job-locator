import React from 'react';
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid';

export default class ManpowerPortal extends React.Component {
  constructor() {
    super()

    this.state = {
      welcomeText: "",
      jobsite: ""
    }

    this.openLink = this.openLink.bind(this)
    
    fetch('getUserInfo')
    .then(res => res.json()) 
    .then(res => {
      this.setState(() => ({
        welcomeText: `Welcome, ${res.displayName}`
      })) 
    })
    
    fetch('getJobsite')
    .then(res => res.text()) 
    .then(res => {
      this.setState(() => ({
        jobsite: res
      })) 
    })
  }

  openLink() {
    window.location.href = `https://maps.google.com/?q=${this.state.jobsite}`
  }

  render() {
    return (
      <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
        >
        <div className="App">
          <h1 id="welcomeId">{this.state.welcomeText}</h1>
          <p>{this.state.jobsite ? "Your job site is" : "No job"}</p>
          <p>{this.state.jobsite}</p>
          <Button
            variant="contained"
            onClick={this.openLink}
          >Send it</Button>
        </div>
      </Grid>
    )
  }
}
