import React from 'react';
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Grid from '@material-ui/core/Grid';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';

export default class AdminPortal extends React.Component {
  constructor() {
    super()
    this.employees = null

    this.state = {
      error: false,
      jobs: [],
      jobInputError: "",
      jobName: "",
      showToast: false,
      toastMessage: "",
      toastSeverity: "success",
      selectedJob: null,
      jobStartDate: new Date(Date.now()).toISOString().substr(0,10),
      jobEndDate: new Date(Date.now()).toISOString().substr(0,10),
      jobLocation: "",
      employees: [],
      selectedEmployees: [],
      employeeSearchQuery: ""
    }

    this.updateJobList()
    fetch('getUsers')
      .then(res => res.json()) 
      .then(res => {
        let newSelectedEmployees = []
        for(let i=0;i<res.length;i++) {
          newSelectedEmployees.push(false)
        }
        this.employees = res
        this.setState(() => ({
          employees: this.employees.map(x => x.name),
          selectedEmployees: newSelectedEmployees
          })) 
      }) 

    this.submit = this.submit.bind(this)
    this.handleClose = this.handleClose.bind(this)
  }

  updateJobList() {
    fetch('jobs')
      .then(res => res.json()) 
      .then(res => {
        this.setState(() => ({jobs: res})) 
      }) 
  }

  async submit() {
    if(this.state.jobName === "") {
      this.setState(() => ({
        error: true,
        jobInputError: "Job cannot be empty"
      }))
    } else {
      this.setState(() => ({
        error: false,
        jobInputError: ""
      }))

      let selectedEmployees = []
      for(let i=0;i<this.state.employees.length;i++) {
        if(this.state.selectedEmployees[i]) {
          selectedEmployees.push(this.employees[i].user_id)
        }
      }

      let res = await fetch(`newJob`, {
        method: 'POST', 
        headers: {
        'Content-Type': 'application/json'
        },
        body: JSON.stringify({
        jobName: this.state.jobName,
        jobStartDate: this.state.jobStartDate,
        jobEndDate: this.state.jobEndDate,
        employees: selectedEmployees,
        jobLocation: this.state.jobLocation
      })})
      this.setState({
        showToast: true,
        toastSeverity: res.ok ? 'success' : 'warning',
        toastMessage: (await res.text())
        })

      this.updateJobList()
      let lastAddedJob = this.state.jobs.length
      this.setState({selectedJob: lastAddedJob})
    }
  }

  handleClose (event, reason) {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({
      showToast: false,
      toastSeverity: '',
      toastMessage: ''
      })
  };

  render() {
    return (
      <div className="App">
        <h1>Jobs</h1>
        <Grid container spacing={2}>
          <Grid name="emptySpace" item xs={3}/>
          <Grid name="jobList" item container direction="column" spacing={3} xs={2}> 
            <Grid item>
              <Button
                  variant="contained"
                  onClick={this.submit}
                >Send it</Button>
              </Grid>
            <Grid item>
              <List>
                {this.state.jobs.map((x, i) => 
                  <ListItem 
                  selected={this.state.selectedJob === i}
                  onClick={(e) => {
                    let jobToShow = this.state.jobs[i]
                    let newSelectedEmployees = []
                    for(let i=0;i<this.employees.length;i++) {
                      newSelectedEmployees.push(jobToShow.employees.includes(this.employees[i].user_id))
                    }

                    this.setState({
                        selectedJob: i,
                        jobName: jobToShow.name,
                        jobStartDate: new Date(jobToShow.startDate).toISOString().substr(0,10),
                        jobEndDate: new Date(jobToShow.endDate).toISOString().substr(0,10),
                        selectedEmployees: newSelectedEmployees,
                        jobLocation: jobToShow.location
                      })
                    }}
                  button>
                    <ListItemText primary={x.name} />
                  </ListItem>
                )}
              </List>
            </Grid>
          </Grid>
          <Grid name="entryCol1" item container direction="column" xs={3} spacing={3}>
            <Grid item>
              <TextField 
                id="newJob" 
                label="Job name"
                value={this.state.jobName}
                onChange={e => this.setState({jobName: e.target.value})}
                error={this.state.error}
                helperText={this.state.jobInputError}/> 
            </Grid>
            <Grid item>
              <TextField
                label="Job start date"
                type="date"
                value={this.state.jobStartDate}
                onChange={e => this.setState({jobStartDate: e.target.value})}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>
            <Grid item>
              <TextField
                  label="Job end date"
                  type="date"
                  value={this.state.jobEndDate}
                  onChange={e => this.setState({jobEndDate: e.target.value})}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
            </Grid>
            <Grid item>
              <TextField 
                  label="Job location"
                  value={this.state.jobLocation}
                  onChange={e => this.setState({jobLocation: e.target.value})}
                  />
            </Grid>
          </Grid>
          <Grid name="entryCol2" item container direction="column" xs={3}>
            <Grid item>
            <TextField 
                  label="Search employee"
                  value={this.state.employeeSearchQuery}
                  onChange={e => this.setState({employeeSearchQuery: e.target.value})}
                  />
            </Grid>
            <Grid item>
              <List dense>
                {this.state.employees.map((x, i) => {
                  if(x.toUpperCase().includes(this.state.employeeSearchQuery.toUpperCase())) {
                    return <ListItem dense button onClick={(e) => {
                      let newSelectedEmployees = this.state.selectedEmployees
                      newSelectedEmployees[i] ^= true
                      this.setState({selectEmployees: newSelectedEmployees})
                      }}>
                      <ListItemIcon>
                        <Checkbox
                          edge="start"
                          checked={this.state.selectedEmployees[i]}
                          tabIndex={-1}
                          disableRipple
                        />
                      </ListItemIcon>
                      <ListItemText  primary={x} />
                    </ListItem>
                  } else {
                    return null
                  }
                })}
              </List>
            </Grid>
          </Grid>
          <Snackbar open={this.state.showToast} autoHideDuration={5000} onClose={this.handleClose}>
            <Alert onClose={this.handleClose} severity={this.state.toastSeverity}>
              {this.state.toastMessage}
            </Alert>
          </Snackbar>
        </Grid>
      </div>
    )
  }
}
