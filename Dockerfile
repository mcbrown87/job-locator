FROM node

COPY app.js ./app/
COPY models ./app/models
COPY package.json ./app/
COPY client ./app/client

WORKDIR /app/client
RUN npm install
RUN npm run-script build

WORKDIR /app

RUN npm install --production

CMD ["npm", "start"]