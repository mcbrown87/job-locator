const mongoose = require('mongoose')

const jobSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    startDate: Date,
    endDate: Date,
    location: String,
    employees: [{
        type: String
    }]
})

module.exports = mongoose.model('Job', jobSchema)